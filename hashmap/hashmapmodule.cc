#include "Python.h"
#include "sparsehash/sparse_hash_map"
#include "sparsehash/sparse_hash_set"
#include <zlib.h>

typedef struct {
  PyObject_HEAD
  google::sparse_hash_map<uint64_t, uint64_t>* hashmap;
} hashmap_Hashmap;

typedef struct {
  PyObject_HEAD
  google::sparse_hash_map<uint64_t, uint64_t>::const_iterator iter;
  hashmap_Hashmap* obj;
} hashmap_HashmapIter;

typedef struct {
  PyObject_HEAD
  google::sparse_hash_map<uint64_t, float>* hashmap;
} hashmap_HashmapUint64Float;

typedef struct {
  PyObject_HEAD
  google::sparse_hash_map<uint64_t, float>::const_iterator iter;
  hashmap_HashmapUint64Float* obj;
} hashmap_HashmapUint64FloatIter;

typedef struct {
  PyObject_HEAD
  google::sparse_hash_set<uint64_t>* hashmap;
} hashmap_Hashset;

typedef struct {
  PyObject_HEAD
  google::sparse_hash_set<uint64_t>::const_iterator iter;
  hashmap_Hashset* obj;
} hashmap_HashsetIter;

static int hashmap_Hashmap_init(hashmap_Hashmap *self, PyObject *args, PyObject *kwds)
{
   self->hashmap = new google::sparse_hash_map<uint64_t, uint64_t>();
   return 0;
}

static int hashmap_HashmapUint64Float_init(hashmap_HashmapUint64Float *self, PyObject *args, PyObject *kwds)
{
   self->hashmap = new google::sparse_hash_map<uint64_t, float>();
   return 0;
}

static int hashmap_Hashset_init(hashmap_Hashset *self, PyObject *args, PyObject *kwds)
{
   self->hashmap = new google::sparse_hash_set<uint64_t>();
   self->hashmap->set_deleted_key(0);
   return 0;
}

static void hashmap_Hashmap_dealloc(hashmap_Hashmap *self) {
  if (self->hashmap) {
	  delete self->hashmap;
	  self->hashmap = NULL;
  }
  self->ob_type->tp_free((PyObject*)self);    
}

static void hashmap_HashmapUint64Float_dealloc(hashmap_HashmapUint64Float *self) {
  if (self->hashmap) {
	  delete self->hashmap;
	  self->hashmap = NULL;
  }
  self->ob_type->tp_free((PyObject*)self);    
}

static void hashmap_Hashset_dealloc(hashmap_Hashset *self) {
  if (self->hashmap) {
	  delete self->hashmap;
	  self->hashmap = NULL;
  }
  self->ob_type->tp_free((PyObject*)self);    
}

static PyObject *
hashmap_Hashmap_len(hashmap_Hashmap *self) {
  return Py_BuildValue("k", self->hashmap->size());
}

static PyObject *
hashmap_HashmapUint64Float_len(hashmap_HashmapUint64Float *self) {
  return Py_BuildValue("k", self->hashmap->size());
}

static PyObject *
hashmap_Hashset_len(hashmap_Hashset *self) {
  return Py_BuildValue("k", self->hashmap->size());
}

template<class K, class V, class C> PyObject* Hashmap_insert(C* self, const char* format, PyObject *args) {
  K key;
  V value;
  if (!PyArg_ParseTuple(args, format, &key, &value))
    return NULL;

  std::pair<typename google::sparse_hash_map<K,V>::iterator,bool> r = self->hashmap->insert(std::pair<K, V>(key, value));
  if (!r.second) {
    // already exists, just update the value
    r.first->second = value;
  }

  PyObject* result = r.second ? Py_True : Py_False;
  Py_INCREF(result);
  return result;
}

static PyObject *
hashmap_Hashmap_insert(hashmap_Hashmap *self, PyObject *args) {
	return Hashmap_insert<uint64_t, uint64_t, hashmap_Hashmap>(self, "KK", args);
}

static PyObject *
hashmap_HashmapUint64Float_insert(hashmap_HashmapUint64Float *self, PyObject *args) {
	return Hashmap_insert<uint64_t, float, hashmap_HashmapUint64Float>(self, "Kf", args);  
}

static PyObject *
hashmap_Hashset_insert(hashmap_Hashset *self, PyObject *args) {
  uint64_t value;
  if (!PyArg_ParseTuple(args, "K", &value))
	return NULL;

  bool r = self->hashmap->insert(value).second;

  PyObject* result = r ? Py_True : Py_False;
  Py_INCREF(result);
  return result;
}

template<class K, class V, class C> PyObject* Hashmap_find(C* self, const char* key_format, const char* value_format, PyObject *args) {
  K key;
  if (!PyArg_ParseTuple(args, key_format, &key))
    return NULL;

  typename google::sparse_hash_map<K, V>::const_iterator it = self->hashmap->find(key);
  if (it == self->hashmap->end()) {
    Py_INCREF(Py_None);
    return Py_None;
  }

  V result = it->second;
  return Py_BuildValue(value_format, result);
}

static PyObject *
hashmap_Hashmap_find(hashmap_Hashmap *self, PyObject *args) {
	return Hashmap_find<uint64_t, uint64_t, hashmap_Hashmap>(self, "K", "K", args);
}

static PyObject *
hashmap_HashmapUint64Float_find(hashmap_HashmapUint64Float *self, PyObject *args) {
	return Hashmap_find<uint64_t, float, hashmap_HashmapUint64Float>(self, "K", "f", args);
}

static PyObject *
hashmap_Hashset_find(hashmap_Hashset *self, PyObject *args) {
  uint64_t value;
  if (!PyArg_ParseTuple(args, "K", &value))
	return NULL;

  google::sparse_hash_set<uint64_t>::const_iterator it = self->hashmap->find(value);
  PyObject* result = it == self->hashmap->end() ? Py_False : Py_True;

  Py_INCREF(result);
  return result;
}

static PyObject *
hashmap_Hashset_erase(hashmap_Hashset *self, PyObject *args) {
  uint64_t value;
  if (!PyArg_ParseTuple(args, "K", &value))
	return NULL;

  self->hashmap->erase(value);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *
hashmap_Hashmap_clear(hashmap_Hashmap *self) {
  self->hashmap->clear();

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *
hashmap_HashmapUint64Float_clear(hashmap_HashmapUint64Float *self) {
  self->hashmap->clear();

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *
hashmap_Hashset_clear(hashmap_Hashset *self) {
  self->hashmap->clear();

  Py_INCREF(Py_None);
  return Py_None;
}

class FileWrapper {
public:
  FILE* fp;
  size_t len;
  uint32_t crc;

  FileWrapper(FILE* f) {
    fp = f;
    len = 0;
    crc = 0;
  }
  size_t Read(void* data, size_t length) {
    size_t r = fread(data, length, 1, fp) * length; 

    // update len and crc
    len += r;
    crc = crc32(crc, (Bytef*)data, r);

    return r;
  }
  size_t Write(const void* data, size_t length) {
    size_t r = fwrite(data, length, 1, fp) * length; 

    // update len and crc
    len += r;
    crc = crc32(crc, (Bytef*)data, r);

    return r;
  }
};

static google::sparse_hash_map<uint64_t, uint64_t>::NopointerSerializer serializer;
static google::sparse_hash_map<uint64_t, float>::NopointerSerializer serializerUint64Float;
static google::sparse_hash_set<uint64_t>::NopointerSerializer serializerUint64;

template<class C, class S> PyObject* Hashmap_serialize(C* self, S& serializer, PyObject *args) {
  PyObject *file;
  if (!PyArg_ParseTuple(args, "O", &file))
    return NULL;

  FILE* f = PyFile_AsFile(file);
  if (!f)
    return NULL;

  FileWrapper fw(f);
  bool b = self->hashmap->serialize(serializer, &fw);

  return Py_BuildValue("OkK", b ? Py_True : Py_False, fw.len, fw.crc);
}


static PyObject *
hashmap_Hashmap_serialize(hashmap_Hashmap *self, PyObject *args) {
  return Hashmap_serialize(self, serializer, args);
}

static PyObject *
hashmap_HashmapUint64Float_serialize(hashmap_HashmapUint64Float *self, PyObject *args) {
  return Hashmap_serialize(self, serializerUint64Float, args);
}

static PyObject *
hashmap_Hashset_serialize(hashmap_Hashset *self, PyObject *args) {
  return Hashmap_serialize(self, serializerUint64, args);
}

template<class C, class S> PyObject* Hashmap_unserialize(C* self, S& serializer, PyObject *args) {
  PyObject *file;
  if (!PyArg_ParseTuple(args, "O", &file))
	return NULL;

  FILE* f = PyFile_AsFile(file);
  if (!f)
	return NULL;

  FileWrapper fw(f);
  bool b = self->hashmap->unserialize(serializer, &fw);

  return Py_BuildValue("OkK", b ? Py_True : Py_False, fw.len, fw.crc);
}

static PyObject *
hashmap_Hashmap_unserialize(hashmap_Hashmap *self, PyObject *args) {
  return Hashmap_unserialize(self, serializer, args);
}

static PyObject *
hashmap_HashmapUint64Float_unserialize(hashmap_HashmapUint64Float *self, PyObject *args) {
  return Hashmap_unserialize(self, serializerUint64Float, args);
}

static PyObject *
hashmap_Hashset_unserialize(hashmap_Hashset *self, PyObject *args) {
  return Hashmap_unserialize(self, serializerUint64, args);
}

static void hashmap_HashmapIter_destruct(hashmap_HashmapIter *self) {
  Py_XDECREF(self->obj);
  self->obj = NULL;
  self->ob_type->tp_free((PyObject*)self);    
}

static void hashmap_HashmapUint64FloatIter_destruct(hashmap_HashmapUint64FloatIter *self) {
  Py_XDECREF(self->obj);
  self->obj = NULL;
  self->ob_type->tp_free((PyObject*)self);    
}

static void hashmap_HashsetIter_destruct(hashmap_HashsetIter *self) {
  Py_XDECREF(self->obj);
  self->obj = NULL;
  self->ob_type->tp_free((PyObject*)self);    
}

static PyObject* hashmap_HashmapIter_iter(PyObject *self)
{
  Py_INCREF(self);
  return self;
}

static PyObject* hashmap_HashmapUint64FloatIter_iter(PyObject *self)
{
  Py_INCREF(self);
  return self;
}

static PyObject* hashmap_HashsetIter_iter(PyObject *self)
{
  Py_INCREF(self);
  return self;
}

template<class K, class V, class C> PyObject* HashmapIter_iternext(C* self, const char* result_format) {
  const google::sparse_hash_map<K, V>* h = self->obj->hashmap;
		  
  if (self->iter != h->end()) {
	PyObject *tmp = Py_BuildValue(result_format, self->iter->first, self->iter->second);
	++(self->iter);
	return tmp;
  } else {
	/* Raising of standard StopIteration exception with empty value. */
	PyErr_SetNone(PyExc_StopIteration);
	return NULL;
  }	
}

template<class K, class C> PyObject* HashsetIter_iternext(C* self, const char* result_format) {
  const google::sparse_hash_set<K>* h = self->obj->hashmap;
		  
  if (self->iter != h->end()) {
	PyObject *tmp = Py_BuildValue(result_format, *self->iter);
	++(self->iter);
	return tmp;
  } else {
	/* Raising of standard StopIteration exception with empty value. */
	PyErr_SetNone(PyExc_StopIteration);
	return NULL;
  }	
}

static PyObject* hashmap_HashmapIter_iternext(hashmap_HashmapIter *self)
{
	return HashmapIter_iternext<uint64_t, uint64_t, hashmap_HashmapIter>(self, "(KK)");
}

static PyObject* hashmap_HashmapUint64FloatIter_iternext(hashmap_HashmapUint64FloatIter *self)
{
	return HashmapIter_iternext<uint64_t, float, hashmap_HashmapUint64FloatIter>(self, "(Kf)");
}

static PyObject* hashmap_HashsetIter_iternext(hashmap_HashsetIter *self)
{
	return HashsetIter_iternext<uint64_t, hashmap_HashsetIter>(self, "K");
}

static PyTypeObject hashmap_HashmapIterType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "hashmap._HashmapIter",            /*tp_name*/
    sizeof(hashmap_HashmapIter),       /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)hashmap_HashmapIter_destruct, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_ITER,
      /* tp_flags: Py_TPFLAGS_HAVE_ITER tells python to
         use tp_iter and tp_iternext fields. */
    "Internal hashmap iterator object.",           /* tp_doc */
    0,  /* tp_traverse */
    0,  /* tp_clear */
    0,  /* tp_richcompare */
    0,  /* tp_weaklistoffset */
    (getiterfunc)hashmap_HashmapIter_iter,  /* tp_iter: __iter__() method */
    (iternextfunc)hashmap_HashmapIter_iternext  /* tp_iternext: next() method */
};

static PyTypeObject hashmap_HashmapUint64FloatIterType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "hashmap._HashmapUint64FloatIter",            /*tp_name*/
    sizeof(hashmap_HashmapUint64FloatIter),       /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)hashmap_HashmapUint64FloatIter_destruct, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_ITER,
      /* tp_flags: Py_TPFLAGS_HAVE_ITER tells python to
         use tp_iter and tp_iternext fields. */
    "Internal hashmap iterator object.",           /* tp_doc */
    0,  /* tp_traverse */
    0,  /* tp_clear */
    0,  /* tp_richcompare */
    0,  /* tp_weaklistoffset */
    (getiterfunc)hashmap_HashmapUint64FloatIter_iter,  /* tp_iter: __iter__() method */
    (iternextfunc)hashmap_HashmapUint64FloatIter_iternext  /* tp_iternext: next() method */
};

static PyTypeObject hashmap_HashsetIterType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "hashmap._HashsetIter",            /*tp_name*/
    sizeof(hashmap_HashsetIter),       /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)hashmap_HashsetIter_destruct, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_ITER,
      /* tp_flags: Py_TPFLAGS_HAVE_ITER tells python to
         use tp_iter and tp_iternext fields. */
    "Internal hashmap iterator object.",           /* tp_doc */
    0,  /* tp_traverse */
    0,  /* tp_clear */
    0,  /* tp_richcompare */
    0,  /* tp_weaklistoffset */
    (getiterfunc)hashmap_HashsetIter_iter,  /* tp_iter: __iter__() method */
    (iternextfunc)hashmap_HashsetIter_iternext  /* tp_iternext: next() method */
};

template<class C, class I> PyObject* Hashmap_iterate(C* self, PyTypeObject* T) {
  /* I don't need python callable __init__() method for this iterator,
	 so I'll simply allocate it as PyObject and initialize it by hand. */

  I* p = PyObject_New(I, T);
  if (!p) return NULL;

  /* I'm not sure if it's strictly necessary. */
  if (!PyObject_Init((PyObject *)p, T)) {
	Py_DECREF(p);
	return NULL;
  }

  p->iter = self->hashmap->begin();
  p->obj = self;
  Py_INCREF(self);
  return (PyObject *)p;
}

static PyObject *
hashmap_Hashmap_iterate(hashmap_Hashmap *self)
{ 
  return Hashmap_iterate<hashmap_Hashmap, hashmap_HashmapIter>(self, &hashmap_HashmapIterType);
}

static PyObject *
hashmap_HashmapUint64Float_iterate(hashmap_HashmapUint64Float *self)
{ 
  return Hashmap_iterate<hashmap_HashmapUint64Float, hashmap_HashmapUint64FloatIter>(self, &hashmap_HashmapUint64FloatIterType);
}

static PyObject *
hashmap_Hashset_iterate(hashmap_Hashset *self)
{ 
  return Hashmap_iterate<hashmap_Hashset, hashmap_HashsetIter>(self, &hashmap_HashsetIterType);
}

static PyMethodDef hashmap_Hashmap_methods[] = {
    {"len",  (PyCFunction)hashmap_Hashmap_len, METH_NOARGS, "hashmap len."},
    {"insert",  (PyCFunction)hashmap_Hashmap_insert, METH_VARARGS, "hashmap insert."},
    {"find",  (PyCFunction)hashmap_Hashmap_find, METH_VARARGS, "hashmap find."},
    {"clear",  (PyCFunction)hashmap_Hashmap_clear, METH_NOARGS, "hashmap clear."},
    {"serialize",  (PyCFunction)hashmap_Hashmap_serialize, METH_VARARGS, "hashmap serialize."},
    {"unserialize",  (PyCFunction)hashmap_Hashmap_unserialize, METH_VARARGS, "hashmap unserialize."},
    {"iterate",   (PyCFunction)hashmap_Hashmap_iterate, METH_NOARGS, "hashmap iterate."},    
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static PyTypeObject hashmap_HashmapType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "hashmap.Hashmap",             /*tp_name*/
    sizeof(hashmap_Hashmap),             /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)hashmap_Hashmap_dealloc, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "Hashmap objects",           /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    hashmap_Hashmap_methods,             /* tp_methods */
    0,		               /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)hashmap_Hashmap_init,      /* tp_init */
};

static PyMethodDef hashmap_HashmapUint64Float_methods[] = {
    {"len",  (PyCFunction)hashmap_HashmapUint64Float_len, METH_NOARGS, "hashmap len."},
    {"insert",  (PyCFunction)hashmap_HashmapUint64Float_insert, METH_VARARGS, "hashmap insert."},
    {"find",  (PyCFunction)hashmap_HashmapUint64Float_find, METH_VARARGS, "hashmap find."},
    {"clear",  (PyCFunction)hashmap_HashmapUint64Float_clear, METH_NOARGS, "hashmap clear."},
    {"serialize",  (PyCFunction)hashmap_HashmapUint64Float_serialize, METH_VARARGS, "hashmap serialize."},
    {"unserialize",  (PyCFunction)hashmap_HashmapUint64Float_unserialize, METH_VARARGS, "hashmap unserialize."},
    {"iterate",   (PyCFunction)hashmap_HashmapUint64Float_iterate, METH_NOARGS, "hashmap iterate."},    
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static PyTypeObject hashmap_HashmapUint64FloatType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "hashmap.HashmapUint64Float",             /*tp_name*/
    sizeof(hashmap_HashmapUint64Float),             /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)hashmap_HashmapUint64Float_dealloc, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "HashmapUint64Float objects",           /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    hashmap_HashmapUint64Float_methods,             /* tp_methods */
    0,		               /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)hashmap_HashmapUint64Float_init,      /* tp_init */
};

static PyMethodDef hashmap_Hashset_methods[] = {
    {"len",  (PyCFunction)hashmap_Hashset_len, METH_NOARGS, "hashmap len."},
    {"insert",  (PyCFunction)hashmap_Hashset_insert, METH_VARARGS, "hashmap insert."},
    {"find",  (PyCFunction)hashmap_Hashset_find, METH_VARARGS, "hashmap find."},
    {"erase",  (PyCFunction)hashmap_Hashset_erase, METH_VARARGS, "hashmap erase."},
    {"clear",  (PyCFunction)hashmap_Hashset_clear, METH_NOARGS, "hashmap clear."},
    {"serialize",  (PyCFunction)hashmap_Hashset_serialize, METH_VARARGS, "hashmap serialize."},
    {"unserialize",  (PyCFunction)hashmap_Hashset_unserialize, METH_VARARGS, "hashmap unserialize."},
    {"iterate",   (PyCFunction)hashmap_Hashset_iterate, METH_NOARGS, "hashmap iterate."},    
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static PyTypeObject hashmap_HashsetType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "hashmap.Hashset",             /*tp_name*/
    sizeof(hashmap_Hashset),             /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)hashmap_Hashset_dealloc, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "Hashset objects",           /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    hashmap_Hashset_methods,             /* tp_methods */
    0,		               /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)hashmap_Hashset_init,      /* tp_init */
};

static PyMethodDef ModuleMethods[] = {   
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
inithashmap(void)
{
	hashmap_HashmapType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&hashmap_HashmapType) < 0)  return;

	hashmap_HashmapUint64FloatType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&hashmap_HashmapUint64FloatType) < 0)  return;

	hashmap_HashsetType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&hashmap_HashsetType) < 0)  return;
	
	hashmap_HashmapIterType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&hashmap_HashmapIterType) < 0)  return;

	hashmap_HashmapUint64FloatIterType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&hashmap_HashmapUint64FloatIterType) < 0)  return;

	hashmap_HashsetIterType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&hashmap_HashsetIterType) < 0)  return;
	
    PyObject* m = Py_InitModule("hashmap", ModuleMethods);
    
    Py_INCREF(&hashmap_HashmapType);
    PyModule_AddObject(m, "Hashmap", (PyObject *)&hashmap_HashmapType);  
    
    Py_INCREF(&hashmap_HashmapUint64FloatType);
    PyModule_AddObject(m, "HashmapUint64Float", (PyObject *)&hashmap_HashmapUint64FloatType);  

    Py_INCREF(&hashmap_HashsetType);
    PyModule_AddObject(m, "Hashset", (PyObject *)&hashmap_HashsetType);  
    
    Py_INCREF(&hashmap_HashmapIterType);
    PyModule_AddObject(m, "_HashmapIter", (PyObject *)&hashmap_HashmapIterType);     

    Py_INCREF(&hashmap_HashmapUint64FloatIterType);
    PyModule_AddObject(m, "_HashmapUint64FloatIter", (PyObject *)&hashmap_HashmapUint64FloatIterType); 
    
    Py_INCREF(&hashmap_HashsetIterType);
    PyModule_AddObject(m, "_HashsetIter", (PyObject *)&hashmap_HashsetIterType);
}
